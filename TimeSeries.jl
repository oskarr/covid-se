using CSV, DataFrames

# Import data from Folkhälsomyndigheten

fhm_s = Vector{DataFrame}();
# Read all S- files in the dir, then add the data to fhm_s
for f in readdir("data/fhm")
    if f[1:2] == "S-"
        push!(fhm_s, rename(DataFrame(CSV.File("data/fhm/" * f))[!, [:Smittland, :Fall]], [:Smittland, Symbol(f[3:8] * f[10:13])]));
    end
end

fhm_s = join(fhm_s..., on = :Smittland, kind = :outer);

# Regional data. fhm_ri is incidence data. The fhm_r is case data.
fhm_r, fhm_ri = Vector{DataFrame}(), Vector{DataFrame}();
for f in readdir("data/fhm")
    if f[1:2] == "R-"
        # Make a dataframe of the file
        local d::DataFrame = DataFrame(CSV.File("data/fhm/" * f));
        # Add the case data to fhm_r
        push!(fhm_r, rename(d[!, [:Region, :Fall]], [:Region, Symbol(f[3:8] * f[10:13])]));
        # If incidence data is present, add it to fhm_ri
        if(:Incidens in names(d))
            push!(fhm_ri, rename(d[!, [:Region, :Incidens]], [:Region, Symbol(f[3:8] * f[10:13])]));
        end
    end
end
fhm_r, fhm_ri = join(fhm_r..., on = :Region, kind = :outer), join(fhm_ri..., on = :Region, kind = :outer);
fhm_r = fhm_r[fhm_r[!, :Region] .!= "Totalt", :];


# Export time series
CSV.write("data/TimeSeries-Region.csv", fhm_r);
CSV.write("data/TimeSeries-Region-Incidence.csv", fhm_ri);
CSV.write("data/TimeSeries-Source.csv", fhm_s);
