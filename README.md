# Covid-SE

Historic epidemiologic data from Folkhälsomyndigheten (Sweden's Public Health Acgency). Inofficial repo.

## Background

Folkhälsomyndigheten publishes new epidemiologic data on the Covid-19 virus every day, but historic data is not available. I've been storing it anyway, and hence this repo came to be. It's updated manually. Hopefully every day.

The data is fetched manually through [their site](https://www.folkhalsomyndigheten.se/smittskydd-beredskap/utbrott/aktuella-utbrott/covid-19/aktuellt-epidemiologiskt-lage/), partially with the help of [archive.org's Wayback Machine](https://www.folkhalsomyndigheten.se/smittskydd-beredskap/utbrott/aktuella-utbrott/covid-19/aktuellt-epidemiologiskt-lage/).

I am not in any way affiliated with Folkhälsomyndigheten (FHM).

## The files

### Source files
The file name contains of a prefix as well as a date on the format `ddmmyy-HHMM` (That is, 010203-0405 means 1st of January 2003, at 04:05 am). The date is when the data was published.

The prefix `R` is short for _Region_, and contains at least the column `Region` and `Fall` (# of cases).
Certain later dates also include the `Incidens` (incidence) metric, which is the number of cases per 100 000 inhabitants.
In addition to the different regions, the tables sometimes contain a _Total_ row, which is preserved as to preserve the total incidence metric for the entirety of Sweden.

The prefix `S` is short for _Smittland_, which roughly translates to "country of infection", and these files only contain two columns. `Smittland` and `Fall` (# of cases).

Also note that the number of rows and columns may vary in between the files.

### Timeseries data
The timeseries data are derived from the source files. The column names are `ddmmyyHHMM`, as well as `Smittland` or `Region`, depending on the file.
